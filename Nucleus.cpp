#include "Nucleus.h"
#include <iostream>

// function to init rna start and end, and init on_complementary_dna_strand
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}
// function checks is_on_complementary_dna_strand?
bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}
// public function to get the start of rna
int Gene::get_start() const
{
	return this->_start;
}
// public function to get the end of rna
int Gene::get_end() const
{
	return this->_end;
}
// public function to init the dna
void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	std::string complementary_dna_strand = "";
	for (unsigned int i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'A')
		{
			complementary_dna_strand += 'T';
		}
		else if (dna_sequence[i] == 'G')
		{
			complementary_dna_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			complementary_dna_strand += 'G';
		}
		else if (dna_sequence[i] == 'T')
		{
			complementary_dna_strand += 'A';
		}
		else
		{
			std::cerr << "dna sequence is not valid" << std::endl;
			_exit(1); // exits with error code
		}
	}
	this->_complementary_DNA_strand = complementary_dna_strand;
}
// function to get the rna transcript
std::string Nucleus::get_RNA_transcript(const Gene & gene) const
{
	std::string _rna_strand = "";
	if (gene.is_on_complementary_dna_strand())
	{
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++)
		{
			if (this->_DNA_strand[i] == 'T')
				_rna_strand += 'U';
			else if (this->_DNA_strand[i] == 'A')
				_rna_strand += 'A';
			else if (this->_DNA_strand[i] == 'G')
				_rna_strand += 'G';
			else if (this->_DNA_strand[i] == 'C')
				_rna_strand += 'C';
			else
			{
				std::cerr << "rna sequence is not valid" << std::endl;
				_exit(1); // exits with error code
			}
		}
	}
	return _rna_strand;
}
// function to get reversed dna
std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string _reversed_dna_strand = "";
	int len = (this->_DNA_strand).length();
	for (int i = len; i >= 0; i--)
	{
		_reversed_dna_strand += this->_DNA_strand[i];
	}
	return _reversed_dna_strand;
}
// public function to get_num_of_codon_appearances
unsigned int Nucleus::get_num_of_codon_appearances(const std::string & codon) const
{
	return this->_DNA_strand.find(codon);
}
