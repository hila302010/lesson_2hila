#ifndef NUCLEUS_H // equivalent to #pragma once
#define NUCLEUS_H //

#include <string>   // for std::string
class Gene
{
public:
	// methods
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	// getters
	bool is_on_complementary_dna_strand() const;
	int get_start() const;
	int get_end() const;
private:
	// fields
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

};

class Nucleus
{
public:
	// methods
	void init(const std::string dna_sequence);
	// getters
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	// fields
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;

};
#endif