#pragma once
#include "Cell.h"
/*
Function will init the dna and gene values
input: string - dna sequence and Gene glucose_receptor_gene
output: None
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_glocus_receptor_gene.init(glucose_receptor_gene.get_start(), glucose_receptor_gene.get_end(), glucose_receptor_gene.is_on_complementary_dna_strand());
	this->_nucleus.init(dna_sequence);
}
/*
Function will load the atp in anergy
input: string - None
output: bool
*/
bool Cell::get_ATP()
{
	std::string rna = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* newProtein = this->_ribosome.create_protein(rna);
	if (newProtein)
	{
		this->_mitochondrion.insert_glucose_receptor(newProtein);
		this->_mitochondrion.set_glucose(50); // set the glucose in 50
		if (this->_mitochondrion.produceATP(50))
		{
			this->_atp_units = 100; // 100 units
			return true;
		}
		else
			return false;
	}
	else
		return false;
}
