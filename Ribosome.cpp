#include "Ribosome.h"
#include <iostream>
using std::cout;
using std::endl;

/*
Function will create a new protein
input: string - rna transcript
output: None
*/
Protein * Ribosome::create_protein(std::string & RNA_transcript) const
{
	std::string codon = ""; // string of codon
	Protein* newProtein = new Protein;
	AminoAcid acid;
	newProtein->init();

	for (int i = 0; i < RNA_transcript.length(); i += LEN_CODON)
	{
		codon = RNA_transcript.substr(i, LEN_CODON); // get first 3 nucleus
		acid = get_amino_acid(codon); 
		if (acid == AminoAcid::UNKNOWN)
			newProtein->clear();
		else
			newProtein->add(acid);
	}
	return newProtein;
}
