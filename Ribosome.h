#pragma once
#ifndef RIBOSOME_H
#define RIBOSOME_H
#include "Protein.h"
#define LEN_CODON 3
class Ribosome
{
public:
	Protein * create_protein(std::string &RNA_transcript) const;
};

#endif // RIBOSOME_H

