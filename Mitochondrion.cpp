#include "Mitochondrion.h"
#include <iostream>
using std::cout;
using std::endl;

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}
/*
Function will check the glucose receptor
input: protein
output: None
*/
void Mitochondrion::insert_glucose_receptor(const Protein * protein)
{
	//ALANINE-LEUCINE-GLYCINE-HISTIDINE-LEUCINE-PHENYLALANINE-AMINO_CHAIN_END
	AminoAcidNode* curr = protein->get_first();
	AminoAcid acid[] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };
	int i = 0, numAcids = 0;
	while (curr) // when curr-> next is NULL - it is the end of the list
	{
		if (curr->get_data() == acid[i])
		{
			numAcids++;
		}
		i++;
		curr = curr->get_next();
	}
	if (i == numAcids) // if the acids above are in the protein
	{
		this->_has_glocuse_receptor = true;
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	if (this->_has_glocuse_receptor == true && this->_glocuse_level >= 50)
		return true;
	else
		return false;
}
